<?php
/*
 * Plugin Twitter v2
 * (c) 2009-2021
 *
 * envoyer et lire des messages de Twitter
 * distribue sous licence GNU/LGPL
 *
 */


if (!defined("_ECRIRE_INC_VERSION")) {
	return;
}

if (!defined("_TWITTER_API_CALL_MICROCACHE_DELAY")) {
	define("_TWITTER_API_CALL_MICROCACHE_DELAY", 180);
}

/**
 * Envoyer un message sur Twitter
 *
 * @param $status
 * @param null $tokens
 *   permet d'utiliser des tokens specifiques et pas ceux pre-configures
 *   (voir twitter_connect)
 *
 * @return string
 *   le status envoye
 */
function tweet($status, $tokens = null) {
	// Certains define prennent le pas sur le reste (mode TEST)
	if (defined('_TEST_MICROBLOG_SERVICE')) {
		spip_log('tweet: desactive par _TEST_MICROBLOG_SERVICE', "twitter" . _LOG_INFO_IMPORTANTE);
		throw new Exception(_T('twitter:erreur_envoi_desactive'));
	}

	// Preparer le message (utf8 < 280 caracteres)
	//$status = trim(preg_replace(',\s+,', ' ', $status));
	$status = trim($status);
	include_spip('inc/charsets');
	$status = unicode2charset(charset2unicode($status), 'utf-8');
	$status = spip_substr($status, 0, 280);

	if (!strlen($status)) {
		spip_log('tweet: rien a envoyer', 'twitter' . _LOG_ERREUR);
		throw new Exception(_T('twitter:erreur_envoi_vide'));
	}

	$datas = [
		'status' => $status
	];

	// anti-begaiment
	$begaie = md5(json_encode(['tokens' => $tokens, 'status' => $status]));

	if ($begaie == $GLOBALS['meta']['twitter_begaie']) {
		spip_log("tweet: on bloque le begaiement de $status", 'twitter' . _LOG_ERREUR);
		throw new Exception(_T('twitter:erreur_envoi_begaie'));
	}

	$options = ($tokens ? $tokens : []);
	$options['return_type'] = 'raw';

	$res = twitter_api_call("statuses/update", 'post', $datas, $options);
	spip_log("tweet: retour = ".json_encode($res), 'twitter' . _LOG_DEBUG);
	if ($res === false) {
		spip_log("tweet: echec lors de l'envoi de $status", 'twitter' . _LOG_ERREUR);
		throw new Exception(_T('twitter:erreur_envoi_technique'));
	}

	if ($res['http_code'] !== 200) {
		spip_log('tweet: Erreur ' . $res['http_code'], 'twitter' . _LOG_ERREUR);
		throw new Exception(_T('twitter:erreur_envoi_technique'));
	}

	// noter l'envoi pour ne pas twitter 2 fois de suite la meme chose
	ecrire_meta('twitter_begaie', $begaie);

	return $status;
}

/**
 * Helper pour les notifications en job_queue : ne pas echouer sur une exception
 * l'erreur sera logee, et la queue pourra continuer
 * @param $status
 * @param null $tokens
 */
function tweet_no_fail($status, $tokens = null) {
	try {
		return tweet($status, $tokens);
	}
	catch (Exception $e) {
		return false;
	}
	return false;
}


/**
 * Charger l'autoload de lib/twitteroauth
 */
function twitter_autoload_lib() {
	if (!class_exists("Composer\\CaBundle")) {
		include_spip('lib/twitteroauth/vendor/composer/ca-bundle/src/CaBundle');
	}
	include_spip('lib/twitteroauth/autoload');
}


/**
 * @param null|array $tokens
 *   twitter_consumer_key : key de l'application a utiliser
 *   twitter_consumer_secret : secret de l'application a utiliser
 *
 *   twitter_account : pour utiliser un compte twitter pre-configure plutot que celui par defaut
 * ou
 *   twitter_token : token du compte a utiliser
 *   twitter_token_secret : token secret du compte a utiliser
 *
 *
 * @return bool|TwitterOAuthSPIP
 */
function twitter_connect($tokens = null) {
	static $connection = null;

	$t = md5(serialize($tokens));
	if (!isset($connection[$t])) {

		if ($tokens = twitter_tokens($tokens)) {
			// Cas de twitter et oAuth
			$t2 = md5(serialize($tokens));

			twitter_autoload_lib();

			try {
				$connection[$t] = $connection[$t2] = new Abraham\TwitterOAuth\TwitterOAuth(
					$tokens['twitter_consumer_key'],
					$tokens['twitter_consumer_secret'],
					$tokens['twitter_token'],
					$tokens['twitter_token_secret']);
			}
			catch (Exception $e) {
				spip_log('Erreur de connexion à twitter, verifier la configuration ' . $e->getMessage(), 'twitter' . _LOG_ERREUR);
				return false;
			}

			if (!$connection[$t2]) {
				spip_log('Erreur de connexion à twitter, verifier la configuration', 'twitter' . _LOG_ERREUR);
				return false;
			}

		}
		else {
			spip_log('Erreur de connexion à twitter, verifier la configuration', 'twitter' . _LOG_ERREUR);
			return false;
		}
	}

	return $connection[$t];
}

/**
 * Determiner les tokens de connexion en fonction de ceux passes
 * et de la configuration par defaut
 *
 * @param array $tokens
 *
 * @return array
 */
function twitter_tokens($tokens = null) {
	$cfg = @unserialize($GLOBALS['meta']['microblog']);
	if (!$cfg AND !$tokens) {
		return false;
	}
	if (!$cfg) {
		$cfg = array();
	}

	if (!is_array($tokens)) {
		$tokens = array();
	}

	$t = array_intersect_key($tokens,
		array(
			'twitter_consumer_key' => '',
			'twitter_consumer_secret' => '',
			'twitter_account' => '',
			'twitter_token' => '',
			'twitter_token_secret' => '',
		));

	if (!isset($t['twitter_consumer_key']) OR !isset($t['twitter_consumer_secret'])) {
		$t['twitter_consumer_key'] = ($cfg['twitter_consumer_key'] ?? '');
		$t['twitter_consumer_secret'] = ($cfg['twitter_consumer_secret'] ?? '');
	}

	if (!isset($t['twitter_token']) OR !isset($t['twitter_token_secret'])) {
		$account = ($cfg['default_account'] ?? '');
		if (isset($t['twitter_account']) AND isset($cfg['twitter_accounts'][$t['twitter_account']])) {
			$account = $t['twitter_account'];
		}

		if (!isset($cfg['twitter_accounts'][$account]) AND isset($cfg['twitter_accounts'])) {
			$accounts = array_keys($cfg['twitter_accounts']);
			$account = reset($accounts);
		}

		if (isset($cfg['twitter_accounts'][$account])) {
			$t['twitter_token'] = $cfg['twitter_accounts'][$account]['token'];
			$t['twitter_token_secret'] = $cfg['twitter_accounts'][$account]['token_secret'];
		}
	}
	if (
		isset($t['twitter_consumer_key'])
		AND isset($t['twitter_consumer_secret'])
		AND isset($t['twitter_token'])
		AND isset($t['twitter_token_secret'])) {
		return $t;
	}

	return false;
}

/**
 * Fonction d'utilisation simple de l'API twitter oAuth
 *
 * @param $command        string : la commande à passer
 * @param $type           string : le type de commande (get/post/delete)
 * @param $params         array : les paramètres dans un array de la commande
 * @param array $options
 *                        bool force : true pour forcer la requete hors cache
 *                        string return_type : le retour souhaité par défaut cela renverra la chaine ou l'array retourné par la commande.
 *                        Sinon on peut utiliser les valeurs http_code,http_info,url
 *
 *   twitter_consumer_key : key de l'application a utiliser
 *   twitter_consumer_secret : secret de l'application a utiliser
 *
 *   twitter_account : pour utiliser un compte twitter pre-configure plutot que celui par defaut
 * ou
 *   twitter_token : token du compte a utiliser
 *   twitter_token_secret : token secret du compte a utiliser
 *
 * @return bool|string|array
 */
function twitter_api_call($command, $type = 'get', $params = array(), $options = null) {

	$api_version = ($options['api_version'] ?? '1.1');
	// API v2 ?
	if (strncmp($command, "2/", 2) === 0) {
		$api_version = 2;
		$command = substr($command, 2);
	}

	spip_log("twitter_api_call v$api_version $type $command " . json_encode($params). " | " . json_encode($options), 'twitter' . _LOG_DEBUG);

	// api_call en cache ?
	$cache_key = null;
	if ($type !== 'get'
		OR (isset($options['force']) AND $options['force'])
		OR !include_spip("inc/memoization")
		OR !function_exists("cache_get")
		OR !$t = twitter_tokens($options)
		OR !$cache_key = "twitter_api_call-" . md5(serialize(json_encode($api_version, $command, $params, $t)))
		OR !$res = cache_get($cache_key)
		OR $res['time'] + _TWITTER_API_CALL_MICROCACHE_DELAY < $_SERVER['REQUEST_TIME']) {

		spip_log("twitter_api_call $type $command : call API", 'twitter' . _LOG_DEBUG);

		if ($connection = twitter_connect($options)) {

			// API version
			$connection->setApiVersion($api_version);

			$res = array();
			try {
				switch ($type) {
					case 'post':
						$res['content'] = $connection->post($command, $params);
						break;
					case 'delete':
						$res['content'] = $connection->delete($command, $params);
						break;
					case 'get':
					default:
						$res['content'] = $connection->get($command, $params);
						break;
				}
				if (!is_string($res['content'])) {
					// on passe le json en array dans tous les cas
					$res['content'] = json_decode(json_encode($res['content']), true);
				}
			}
			catch (Exception $e) {
				spip_log("twitter_api_call $type $command : ECHEC " . $e->getMessage(), 'twitter' . _LOG_ERREUR);
				return false;
			}

			$res['http_code'] = $connection->getLastHttpCode();
			$res['http_info'] = $connection->getLastXHeaders();
			$res['url'] = $connection->getLastApiPath();
			$res['time'] = $_SERVER['REQUEST_TIME'];

			spip_log("twitter_api_call $type $command : res " . json_encode($res), 'twitter' . _LOG_DEBUG);

			// si la reponse est une erreur en json, ne pas faire croire que tout va bien et ne pas stocker en cache
			if (!empty($res['content']['errors'])) {
				spip_log("twitter_api_call $type $command : ECHEC " . json_encode($res['content']['errors']), 'twitter' . _LOG_ERREUR);
				return false;
			}

			if ($cache_key) {
				cache_set($cache_key, $res, _TWITTER_API_CALL_MICROCACHE_DELAY * 2);
			}
		} else {
			if (!$res) {
				spip_log("twitter_api_call $type $command : echec connexion, pas de vieux cache en fallback", "twitter" . _LOG_ERREUR);
				return false;
			}
			else {
				spip_log("twitter_api_call $type $command : echec connexion, on utilise le cache perime", "twitter" . _LOG_ERREUR);
			}
		}
	}

	$retour = (isset($options['return_type']) ? $options['return_type'] : 'content');
	if ($retour !== 'raw' and !isset($res[$retour])) {
		$retour = 'content';
	}

	switch ($retour) {
		case 'content':
			return $res['content'];
		case 'raw':
			return $res;
		default:
			return $res[$retour];
	}
}

/**
 * Verifier que la config twitter est OK
 *
 * @param bool $complete
 *   verification complete de la connexion, avec requete chez Twitter (plus lent)
 *
 * @return bool
 */
function twitter_verifier_config($complete = false) {
	if (!$tokens = twitter_tokens()) {
		return false;
	}
	if ($complete) {
		if (!twitter_connect()) {
			return false;
		}
		if (!$infos = twitter_api_call("account/verify_credentials")) {
			return false;
		}
	}

	return true;
}