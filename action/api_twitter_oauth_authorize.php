<?php
/*
 * Plugin Twitter v2
 * (c) 2009-2021
 *
 * envoyer et lire des messages de Twitter
 * distribue sous licence GNU/LGPL
 *
 */


if (!defined("_ECRIRE_INC_VERSION")) {
	return;
}

/**
 * Fonction vérifiant le retour de twitter
 * Elle met dans la configuration du plugin les tokens
 * nécessaires pour de futures connexions
 */
function action_api_twitter_oauth_authorize_dist() {

	include_spip('inc/twitter');
	include_spip('inc/session');

	$redirect = session_get('twitter_redirect') ? session_get('twitter_redirect') : $GLOBALS['meta']['url_site_spip'];
	if (!empty($GLOBALS['visiteur_session']['id_auteur'])
		and !empty($GLOBALS['visiteur_session']['oauth_token'])) {

		if (_request('denied')) {
			spip_log("action_twitter_oauth_authorize_dist : denied", 'twitter' . _LOG_ERREUR);
			$redirect = parametre_url($redirect, 'erreur', 'auth_denied', '&');
			session_set('oauth_status', 'denied');
			$GLOBALS['redirect'] = $redirect;
		} elseif (_request('oauth_token') && ($GLOBALS['visiteur_session']['oauth_token'] !== _request('oauth_token'))) {
			spip_log("action_twitter_oauth_authorize_dist : old_token", 'twitter' . _LOG_ERREUR);
			$redirect = parametre_url($redirect, 'erreur', 'old_token', '&');
			session_set('oauth_status', 'oldtoken');
			$GLOBALS['redirect'] = $redirect;
		} else {
			$cfg = @unserialize($GLOBALS['meta']['microblog']);
			$consumer_key = $cfg['twitter_consumer_key'];
			$consumer_secret = $cfg['twitter_consumer_secret'];

			twitter_autoload_lib();

			try {
				$connection = new Abraham\TwitterOAuth\TwitterOAuth(
					$consumer_key,
					$consumer_secret,
					$GLOBALS['visiteur_session']['oauth_token'],
					$GLOBALS['visiteur_session']['oauth_token_secret']
				);
			}
			catch (Exception $e) {
				spip_log("action_twitter_oauth_authorize_dist : erreur lors de la tentative de connexion : " . $e->getMessage(), 'twitter' . _LOG_ERREUR);
				$redirect = parametre_url($redirect, 'erreur', 'connexion_failed', '&');
				session_set('oauth_status', 'connexion_failed');
				$GLOBALS['redirect'] = $redirect;
				twitter_oauth_clean_session();
				return;
			}

			$auth_verifier = _request('oauth_verifier');
			if (empty($auth_verifier)) {
				spip_log("action_twitter_oauth_authorize_dist : empty oauth_verifier", 'twitter' . _LOG_ERREUR);
				$redirect = parametre_url($redirect, 'erreur', 'connexion_failed', '&');
				session_set('oauth_status', 'connexion_failed');
				$GLOBALS['redirect'] = $redirect;
				twitter_oauth_clean_session();
				return;
			}

			$access_token = $connection->oauth("oauth/access_token", ["oauth_verifier" => $auth_verifier]);
			$http_status = $connection->getLastHttpCode();
			spip_log("action_twitter_oauth_authorize_dist : oauth/access_token Status $http_status | ".json_encode($access_token), 'twitter' . _LOG_DEBUG);
			session_set('access_token', $access_token);

			/**
			 * Si le code de retour est 200 :
			 * L'utilisateur a été vérifié et
			 * les tokens d'accès peuvent être sauvegardés pour un usage futur
			 * on appelle la callback en session qui en fait ce qu'elle veut
			 */
			if (200 == $http_status) {

				if ($callback = session_get('twitter_callback')
					AND $callback = charger_fonction($callback, "action", true)) {
					// si la callback retourne quelque chose c'est une url de redirect
					if ($r = $callback(true, $redirect)) {
						$redirect = $r;
					}
				}

				$GLOBALS['redirect'] = $redirect;
			} else {
				spip_log("Erreur HTTP Status '" . $http_status . "' au retour pour recuperation des tokens dans action_twitter_oauth_callback_dist", 'twitter' . _LOG_ERREUR);
				// peut donner une info en plus, genre un message d'erreur a la place des tokens
				$redirect = parametre_url($redirect, 'erreur_code', $http_status);
				if (count($access_token) == 1
					AND $e = trim(implode(" ", array_keys($access_token)) . " " . implode(" ", array_values($access_token)))) {
					session_set("oauth_erreur_message", "Erreur : $e");
					$redirect = parametre_url($redirect, 'erreur', 'erreur_oauth', '&');
				} else {
					$redirect = parametre_url($redirect, 'erreur', 'auth_denied', '&');
				}
				$GLOBALS['redirect'] = $redirect;
			}
		}
	} else {
		// rien a faire ici !
		$GLOBALS['redirect'] = $redirect;
	}

	twitter_oauth_clean_session();
}

/**
 * Vider la session des jetons/redirect et callback temporaires
 */
function twitter_oauth_clean_session() {
	foreach (array('access_token', 'oauth_token', 'oauth_token_secret', 'twitter_redirect', 'twitter_callback') as $k) {
		if (isset($GLOBALS['visiteur_session'][$k])) {
			session_set($k);
		}
	}
}

function twitter_oauth_authorize($callback, $redirect, $sign_in = true) {
	$cfg = @unserialize($GLOBALS['meta']['microblog']);

	$redirect = parametre_url(parametre_url($redirect, 'erreur_code', ''), 'erreur', '', '&');

	include_spip('inc/filtres');
	include_spip('inc/twitter');
	include_spip('twitter_fonctions');
	include_spip('inc/session');

	/**
	 * L'URL de callback en mode .api pour eviter les query strings
	 * Elle vérifiera le retour et finira la configuration
	 */
	$oauth_callback = twitter_url_callback_authorize();
	twitter_autoload_lib();

	$erreur = '';
	/**
	 * Récupération des tokens depuis twitter par rapport à notre application
	 * On les place dans la session de l'individu en cours
	 * Ainsi que l'adresse de redirection pour la seconde action
	 */
	spip_log("twitter_oauth_authorize : lance une demande de jeton avec callback url $oauth_callback", 'twitter' . _LOG_DEBUG);
	try {
		$connection = new Abraham\TwitterOAuth\TwitterOAuth(
			$cfg['twitter_consumer_key'],
			$cfg['twitter_consumer_secret']
		);

		$request_token = $connection->oauth('oauth/request_token', ['oauth_callback' => $oauth_callback]);
		spip_log("twitter_oauth_authorize : reponse " . json_encode($request_token), 'twitter' . _LOG_DEBUG);

		if (empty($request_token['oauth_token']) or empty($request_token['oauth_token_secret'])) {
			session_set('oauth_erreur_message', "Pas de oauth_token|oauth_token_secret en reponse de oauth/request_token");
			$redirect = parametre_url($redirect, 'erreur', "erreur_oauth", '&');
			$GLOBALS['redirect'] = $redirect;
			return;
		}

		$token = $request_token['oauth_token'];
		session_set('oauth_token', $token);
		session_set('oauth_token_secret', $request_token['oauth_token_secret']);
		// on stocke le redirect en absolue car au retour on sera sur une .api publique et le redirect est surement dans le prive
		$redirect_abs = url_absolue(str_replace('&amp;', '&', $redirect));
		session_set('twitter_redirect', $redirect_abs);
		session_set('twitter_callback', $callback);

		/**
		 * Vérification du code de retour
		 */
		switch ($code = $connection->getLastHttpCode()) {
			/**
			 * Si le code de retour est 200 (ok)
			 * On envoie l'utilisateur vers l'url d'autorisation
			 */
			case 200:
				$url = $connection->url($sign_in ? "oauth/authenticate" : "oauth/authorize", ["oauth_token" => "$token"]);
				include_spip('inc/headers');
				$GLOBALS['redirect'] = $url;
				spip_log("twitter_oauth_authorize : URL authorize $url", 'twitter' . _LOG_DEBUG);
				break;

			/**
			 * Sinon on le renvoie vers le redirect avec une erreur
			 */
			default:
				spip_log('twitter_oauth_authorize: Erreur connexion twitter', 'twitter' . _LOG_ERREUR);
				spip_log($connection, 'twitter' . _LOG_ERREUR);
				$redirect = parametre_url($redirect, 'erreur_code', $code);
				$redirect = parametre_url($redirect, 'erreur', 'erreur_conf_app', '&');
				$GLOBALS['redirect'] = $redirect;
				break;
		}
	} catch (Exception $e) {
		spip_log("twitter_oauth_authorize : echec oauth/request_token" . $e->getMessage(), 'twitter' . _LOG_DEBUG);
		session_set('oauth_erreur_message', $e->getMessage());
		$redirect = parametre_url($redirect, 'erreur', "erreur_oauth", '&');
		$GLOBALS['redirect'] = $redirect;
	}
}

