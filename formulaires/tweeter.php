<?php
/*
 * Plugin Twitter v2
 * (c) 2009-2021
 *
 * envoyer et lire des messages de Twitter
 * distribue sous licence GNU/LGPL
 *
 */


if (!defined("_ECRIRE_INC_VERSION")) {
	return;
}

/**
 * Fonction de chargement des valeurs par defaut des champs du formulaire
 */
function formulaires_tweeter_charger_dist(){
	$valeurs = [
		'status' => '',
	];

	include_spip("inc/twitter");
	if (!twitter_verifier_config()){
		$valeurs['editable'] = false;
		$valeurs['message_erreur'] = _T('twitter:erreur_config_pour_tweeter');
	}

	return $valeurs;
}

/**
 * Fonction de vérification du formulaire avant traitement
 *
 * Vérifie la présence d'un statut depuis le champs adéquat
 * Vérifie que la longueur du statut n'excède pas la longueur maximale
 */
function formulaires_tweeter_verifier_dist(){
	include_spip('inc/charsets');
	$erreurs = [];
	if (!$status = trim(_request('status'))){
		$erreurs['status'] = _T('info_obligatoire');
	}
	elseif (spip_strlen($status)>280) {
		$erreurs['status'] = _T('twitter:longueur_maxi_status');
	}

	return $erreurs;
}

/**
 * Fonction de traitement du formulaire
 * Envoie la contribution au service configuré
 *
 * S'il y a une erreur en retour (false),
 * on affiche un message explicitant qu'il y a une erreur dans la configuration
 */
function formulaires_tweeter_traiter_dist(){
	$res = ['editable' => true];

	$status = _request('status');
	include_spip('inc/twitter');

	try {
		$status_envoye = tweet($status);
	}
	catch (Exception $e) {
		$erreur = $e->getMessage();
		$res['message_erreur'] = $erreur;
		return $res;
	}


	// on vide l'invite de status
	set_request('status', '');
	$res['message_ok'] = _T('twitter:message_envoye')
		. "<blockquote class='explication tweet'>".nl2br($status_envoye)."</blockquote>";

	return $res;
}


