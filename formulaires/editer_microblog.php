<?php
/*
 * Plugin Twitter v2
 * (c) 2009-2021
 *
 * envoyer et lire des messages de Twitter
 * distribue sous licence GNU/LGPL
 *
 */


if (!defined("_ECRIRE_INC_VERSION")) {
	return;
}


/**
 * Fonction de chargement des valeurs par defaut des champs du formulaire
 */
function formulaires_editer_microblog_charger_dist($objet, $id_objet, $hide_form = false) {
	$primary = id_table_objet($objet);

	$valeurs = array();
	$valeurs['_hide'] = (($hide_form AND is_null(_request('status'))) ? ' ' : '');
	$valeurs['objet'] = $objet;
	$valeurs['id_objet'] = $id_objet;
	$valeurs['status'] = recuperer_fond("modeles/microblog_instituer" . $objet, array($primary => $id_objet));
	$valeurs['_status'] = trim($valeurs['status']);

	include_spip("inc/twitter");
	if (!twitter_verifier_config()) {
		$valeurs['editable'] = false;
		$valeurs['message_erreur'] = _T('twitter:erreur_config_pour_tweeter');
	}

	return $valeurs;
}

/**
 * Fonction de vérification du formulaire avant traitement
 *
 * Vérifie la présence d'un statut depuis le champs adéquat
 * Vérifie que la longueur du statut n'excède pas la longueur maximale
 */
function formulaires_editer_microblog_verifier_dist($objet, $id_objet) {
	include_spip('inc/charsets');

	$erreurs = [];

	$microblog = _request('status');
	if (spip_strlen($microblog) > 280) {
		$erreurs['status'] = _T('twitter:longueur_maxi_status');
	}

	return $erreurs;
}

/**
 * Fonction de traitement du formulaire
 * Envoie la contribution au service configuré
 *
 * S'il y a une erreur en retour (false),
 * on affiche un message explicitant qu'il y a une erreur dans la configuration
 */
function formulaires_editer_microblog_traiter_dist($objet, $id_objet) {

	$res = ['editable' => true];

	$status = _request('status');

	if (_request('annuler_microblog')) {
		// ruse pour ne rien envoyer
		$status = " ";
	}

	if (!is_null($status)) {
		$set = [
			'microblog' => $status
		];

		include_spip('action/editer_objet');
		objet_modifier($objet, $id_objet, $set);
	}

	if (!strlen(trim($status))) {
		set_request('status');
	}

	if (_request('envoyer')) {

		include_spip('inc/twitter');

		try {
			$status_envoye = tweet($status);
		}
		catch (Exception $e) {
			$erreur = $e->getMessage();
			$res['message_erreur'] = $erreur;
			return $res;
		}

		// on vide l'invite de status
		$res['message_ok'] = _T('twitter:message_envoye')
			. "<blockquote class='explication tweet'>".nl2br($status_envoye)."</blockquote>";

	}

	return $res;
}


